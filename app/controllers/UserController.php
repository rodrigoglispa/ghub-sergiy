<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 8/20/14
 * Time: 3:16 PM
 */

class UserController {

    protected $user ='';

    public function __construct(\User $user) {
        $this->user = $user;
    }

    public function get_users() {

        $us = $this->user;
        $users = $us::all();
        return $users->toJson();
    }

    public function save_user($name) {
        $user = $this->user;
        $user->user_name = $name;

        $user->save();

//        return $user->toArray();
        return $user->toJson();
    }

}