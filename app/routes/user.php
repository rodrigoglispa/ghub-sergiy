<?php

$app->container->singleton('userController', function () {
    return new UserController(new User());
});


$app->get('/save/:name', function($name) use ($app) {
    // Fetch all users
    $app->contentType('application/json');
    echo $app->userController->save_user($name);

});

$app->get('/get_user','authenticate', function () use ($app) {

    $app->contentType('application/json');

    echo $app->userController->get_users();

});


/**
 * Adding Middle Layer to authenticate every request
 * Checking if the request has valid api key in the 'Authorization' header
 */
function authenticate(\Slim\Route $route) {
    // Getting request headers
    $headers = apache_request_headers();

    $app = \Slim\Slim::getInstance();
}