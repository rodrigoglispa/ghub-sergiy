Setting up the dependencies

Build the package as described in https://bitbucket.org/jonmolin/php-testing-pkg and install the package
install ant (sudo apt-get install ant)

Run 'ant test' in the project top dir to run the tests



PHPUNIT UNDER PHPStorm

1.- How to install PHPUnit

    - Download .phar file from phpunit.com (Download verion 3.7.*, further versions do nto work fine with PHPStorm)

    Follow the instructions (Running PHPUnit from the phpunit.phar archive ):
    http://www.jetbrains.com/phpstorm/webhelp/enabling-phpunit-support.html

2.- Include Libraries

    - Include .phar in libraries to use PHPStorm autocomplete

    - File -> Settings
    - Go to PHP tab.
    - In include path select the path of the folder where .phar file is stored.

3.- Create a run config

    Follow the instructions:
    http://www.jetbrains.com/phpstorm/webhelp/run-debug-configuration-phpunit.html
