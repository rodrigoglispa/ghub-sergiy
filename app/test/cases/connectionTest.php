<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 8/20/14
 * Time: 9:14 AM
 */

//namespace app\test\cases;

class connectionTest extends \PHPUnit_Framework_TestCase {

    public $connection = '';

    /**
     * Runs one before each test
     */
    public function setUp() {
        try{
            require_once  dirname(dirname(dirname(__DIR__))) . '/vendor/autoload.php';
            require_once  dirname(dirname(dirname(__DIR__))) . '/config/connection.php';
            require_once  dirname(dirname(__DIR__)) . '/models/User.php';
            require_once  dirname(dirname(__DIR__)) . '/controllers/UserController.php';
        } catch(\Exception $ex) {
            $this->connection = $ex;
        }
    }

    public function testStartConnection() {
        $this->assertInternalType('string', $this->connection);
    }


    public function testModelUserJson() {
        $users = \User::all();
        $this->assertJson($users->toJson());
    }


    public function testTableUserNotEmpty() {
        $users = \User::all();
        $this->assertGreaterThan(0,$users->count(),'ERROR: The user table is empty, with '.$users->count().' records found.');
    }


    public function test2() {
        // we mock the user and we send that to the controller
        // We are testing the controller with out calleing the DB
        // so we need a model with no DB to test the insert
        // Creating the mock for the User model

        $mock_user_model = $this->getMockBuilder('User')
            ->setMethods(array('save'))
            ->getMock();


        // Declare the Fake save function
        $mock_save_call =  function() use ($mock_user_model) {
            $mock_user_model->id = 1;
            return $mock_user_model;
        };

        // Override the save function with the callback
        $mock_user_model->expects($this->once())
                ->method('save')
                ->will($this->returnCallback($mock_save_call));

        // Crate instance of the controller with the Mock_User class
        $uc = New \UserController($mock_user_model);

        $name = 'test_jos';

        // Call the save_user in the controller.
        // This will call the save function in the controller that is being override by the mock User Save
        $this->assertEquals('{"user_name":"' . $name . '","id":1}',$uc->save_user($name));

    }
}