<?php

// Database information
$settings = array(
    'driver'    => 'mysql',
    'host'      => 'localhost',
    //'port'      => 3306,
    'database'  => 'slimtest',
    'username'  => 'root',
    'password'  => '',
    'prefix'    => '',
    'charset'   => "utf8",
    'collation' => "utf8_general_ci",
    'profiler' => true,
);

// Bootstrap Eloquent ORM
$connFactory = new \Illuminate\Database\Connectors\ConnectionFactory(new \Illuminate\Container\Container);
$conn = $connFactory->make($settings);
$resolver = new \Illuminate\Database\ConnectionResolver();
$resolver->addConnection('default', $conn);
$resolver->setDefaultConnection('default');
\Illuminate\Database\Eloquent\Model::setConnectionResolver($resolver);