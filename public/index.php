<?php

set_include_path(dirname(__DIR__) . PATH_SEPARATOR . dirname(__FILE__) . PATH_SEPARATOR . get_include_path());

date_default_timezone_set('EST');

require 'vendor/autoload.php';
require 'config/connection.php';

$app = new \Slim\Slim();

/*
 * Autoload for all models
 */
function my_autoload ($pClassName) {
    $pClassName = preg_replace('/Controller/','',$pClassName);

    require 'app/models/'.$pClassName.'.php';
    require 'app/controllers/'.$pClassName.'Controller.php';

}
spl_autoload_register("my_autoload");

/*
 * All routes
 */
if ($handle = opendir(dirname(__DIR__) . '/app/routes')) {
    /* This is the correct way to loop over the directory. */
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {

            require_once 'app/routes/' . $entry;

        }
    }

    closedir($handle);
}

$app->run();